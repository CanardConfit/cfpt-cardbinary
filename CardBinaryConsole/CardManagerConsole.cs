﻿using System;
using System.Collections.Generic;
using System.Linq;
using CardBinary.Cards;

namespace CardBinaryConsole
{
    public class CardManagerConsole : CardManager
    {
        private int _actualValue = 255;
        private readonly List<Card> _cards;

        public CardManagerConsole()
        {
            _cards = new List<Card>();

            ActualMode = Mode.Manuel;

            for (var bin = 1; bin <= 128; bin += bin)
                _cards.Add(new Card(this, bin));
        }

        public override Mode ActualMode { get; set; }
        public override List<Card> Cards => _cards;
        public override object ObjectRetourner => null;

        public override int ActualValue => _actualValue;

        public override void RetourneAll()
        {
            Cards.ForEach(card => card.Retourner = true);
            _actualValue = 0;
        }

        public override object RetourneCard(int value)
        {
            var card = _cards.First(el => el.Value == value);
            if (card == null) throw new InvalidOperationException("La valeur n'est une carde valide !");

            card.Retourner = !card.Retourner;

            _actualValue += card.Retourner ? -card.Value : card.Value;

            return null;
        }
    }
}