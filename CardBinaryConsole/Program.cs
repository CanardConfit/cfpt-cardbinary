﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using CardBinary.Cards;

namespace CardBinaryConsole
{
    public class Program
    {
        private static readonly int[] _list = {128, 64, 32, 16, 8, 4, 2, 1};

        private readonly CardManager _manager;
        private readonly bool _stop = true;

        private StringBuilder _affBuilder;

        private readonly int _curVal;
        private readonly int _maxVal = 63;

        private Program()
        {
            _manager = new CardManagerConsole();

            InitTemplate();

            while (_stop)
                try
                {
                    Affiche();

                    if (_manager.ActualMode == Mode.Manuel)
                    {
                        Console.Write("Entrez un chiffre (128,64,32,16,8,4,2,1) ou \"mode auto|manuel|exit\" : ");
                        var line = Console.ReadLine();

                        if (line != null)
                        {
                            if (line.StartsWith("mode "))
                            {
                                switch (line.Replace("mode ", ""))
                                {
                                    case "auto":
                                        if (_manager.ActualMode == Mode.Automatique)
                                            throw new ArgumentException("Ce mode est déjà activé !");
                                        _manager.ActualMode = Mode.Automatique;
                                        break;
                                    case "manuel":
                                        if (_manager.ActualMode == Mode.Manuel)
                                            throw new ArgumentException("Ce mode est déjà activé !");
                                        _manager.ActualMode = Mode.Manuel;
                                        break;
                                    case "exit":
                                        _stop = false;
                                        break;
                                    default:
                                        throw new ArgumentException("Mode non reconnu ! (auto|manuel)");
                                }

                                Console.WriteLine("Changement du mode fait !");
                                continue;
                            }

                            if (line.ToCharArray().Any(c => !char.IsNumber(c)))
                                throw new ArgumentException("Le chiffre entré n'est pas un chiffre !");

                            var cardId = Convert.ToInt32(line);

                            // Verification que le chiffre soit un multiple de 2 (Principe n & (n-1))
                            if (cardId < 0 || (cardId & (cardId - 1)) == 1)
                                throw new ArgumentException("Le chiffre n'est pas un multiple de 2 !");

                            if (cardId > 128) throw new ArgumentException("Le chiffre est trop élevé !");

                            _manager.RetourneCard(cardId);
                        }
                    }
                    else if (_manager.ActualMode == Mode.Automatique)
                    {
                        var tmp = _curVal;
                        _manager.RetourneAll();
                        for (var bin = 128; bin >= 1; bin -= bin > 1 ? bin / 2 : 1)
                            if (tmp - bin >= 0)
                            {
                                tmp -= bin;
                                _manager.RetourneCard(bin);
                            }

                        _curVal += 1;

                        if (_curVal > _maxVal) _manager.ActualMode = Mode.Manuel;

                        Thread.Sleep(1000);
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine($"ERREUR : {e.Message}");
                    Thread.Sleep(1000);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Une erreur inattendue est survenue : {e.Message}");
                    _stop = false;
                }
        }

        private static void Main(string[] args)
        {
            var prog = new Program();
        }

        private void InitTemplate()
        {
            _affBuilder = new StringBuilder();
            _affBuilder.AppendLine("---------------------------------------------------------------");
            _affBuilder.AppendLine(
                "   [bin128]       [bin64]       [bin32]       [bin16]       [bin8]       [bin4]       [bin2]       [bin1]   ");
            _affBuilder.AppendLine("------- ------- ------- ------- ------- ------- ------- -------");
            _affBuilder.AppendLine(
                "| [aff128] | | [aff64] | | [aff32] | | [aff16] | | [aff8] | | [aff4] | | [aff2] | | [aff1] |");
            _affBuilder.AppendLine("------- ------- ------- ------- ------- ------- ------- -------");
            _affBuilder.AppendLine(" ");
            _affBuilder.AppendLine(
                "  [val128]  +  [val64]  +  [val32]  +  [val16]  +  [val8]  +  [val4]  +  [val2]  +  [val1] =");
            _affBuilder.AppendLine(" ");
            _affBuilder.AppendLine("                              [result]");
            _affBuilder.AppendLine(" ");
            _affBuilder.AppendLine("                       [mode]");
            _affBuilder.AppendLine("---------------------------------------------------------------");
        }

        private void Affiche()
        {
            var curBuilder = new StringBuilder(_affBuilder.ToString());

            for (var bin = 1; bin <= 128; bin += bin)
            {
                var card = _manager.Cards.First(c => c.Value == bin);

                curBuilder.Replace($"[bin{bin}]", card.Retourner ? "0" : "1");
                curBuilder.Replace($"[aff{bin}]", card.Retourner ? " - " : $"{card.Value,3:###}");
                curBuilder.Replace($"[val{bin}]", card.Retourner ? " 0 " : $"{card.Value,3:###}");
            }

            curBuilder.Replace("[result]", _manager.ActualValue.ToString());
            curBuilder.Replace("[mode]", _manager.ActualMode.ToString());

            Console.WriteLine(curBuilder.ToString());
        }
    }
}