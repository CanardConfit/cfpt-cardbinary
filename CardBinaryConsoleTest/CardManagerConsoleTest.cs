using CardBinary.Cards;
using CardBinaryConsole;
using NUnit.Framework;

namespace CardBinaryTest
{
    public class CardManageConsoleTest
    {
        private CardManager _manager;
        
        [SetUp]
        public void Setup()
        {
            _manager = new CardManagerConsole();
        }

        [Test]
        public void Assert_CardManager_Return_Cards()
        {
            Assert.IsTrue(_manager.Cards.TrueForAll(card => !card.Retourner));

            for (var bin = 1; bin <= 128; bin += bin)
                _manager.RetourneCard(bin);
                    
            Assert.IsTrue(_manager.Cards.TrueForAll(card => card.Retourner));
        }
    }
}