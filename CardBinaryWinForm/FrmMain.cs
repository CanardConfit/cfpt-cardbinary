﻿using System;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;
using CardBinary.Cards;

namespace CardBinaryWinForm
{
    public partial class FrmMain : Form
    {
        private readonly CardManager _cardManager;
        private int _curValue;
        private readonly int _maxValue = 63;

        public FrmMain()
        {
            InitializeComponent();

            _cardManager = new CardManagerWinForm(gbxCards.Controls);

            UpdateAffichage();
        }

        private void card_Click(object sender, EventArgs e)
        {
            if (_cardManager.ActualMode == Mode.Manuel)
            {
                var pbx = sender as PictureBox;
                var id = pbx.Name.Replace("pbxCard", "");

                int value;

                if (int.TryParse(id, out value)) pbx.Image = (Image) _cardManager.RetourneCard(value);

                UpdateAffichage();
            }
        }

        private void UpdateAffichage()
        {
            foreach (var card in _cardManager.Cards)
            {
                ((Label) gbxCalcul.Controls.Find($"lblCalc{card.Value}", false)[0]).Text =
                    card.Retourner ? "0" : card.Value.ToString();
                ((Label) gbxBin.Controls.Find($"lblBin{card.Value}", false)[0]).Text = card.Retourner ? "0" : "1";
            }

            lblCalcResult.Text = _cardManager.ActualValue.ToString();

            lblMode.Text = _cardManager.ActualMode.ToString();
        }

        private void tsms_Click(object sender, EventArgs e)
        {
            var btn = sender as ToolStripMenuItem;

            switch (btn.Name)
            {
                case "tsmManuel":
                    _cardManager.ActualMode = Mode.Manuel;
                    mainTimer.Stop();
                    break;
                case "tsmAuto":
                    _cardManager.ActualMode = Mode.Automatique;
                    _curValue = 0;
                    mainTimer.Start();
                    break;
                case "tsmRetourner":
                    RetourneAll();
                    break;
            }

            UpdateAffichage();
        }

        private void RetourneAll()
        {
            _cardManager.RetourneAll();

            for (var bin = 1; bin <= 128; bin += bin)
                RetourneImage(bin);
        }

        private void RetourneImage(int value)
        {
            var c = _cardManager.Cards.Find(card => card.Value == value);
            ((PictureBox) gbxCards.Controls.Find($"pbxCard{value}", false)[0]).Image =
                c.CurrentImage;
        }

        private void mainTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var tmp = _curValue;
            RetourneAll();
            for (var bin = 128; bin >= 1; bin -= bin > 1 ? bin / 2 : 1)
                if (tmp - bin >= 0)
                {
                    tmp -= bin;
                    _cardManager.RetourneCard(bin);
                    RetourneImage(bin);
                }

            _curValue += 1;

            if (_curValue > _maxValue)
            {
                _cardManager.ActualMode = Mode.Manuel;
                mainTimer.Stop();
            }

            UpdateAffichage();
        }
    }
}