﻿namespace CardBinaryWinForm
{
    partial class FrmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxBin = new System.Windows.Forms.GroupBox();
            this.lblBin1 = new System.Windows.Forms.Label();
            this.lblBin2 = new System.Windows.Forms.Label();
            this.lblBin4 = new System.Windows.Forms.Label();
            this.lblBin8 = new System.Windows.Forms.Label();
            this.lblBin32 = new System.Windows.Forms.Label();
            this.lblBin64 = new System.Windows.Forms.Label();
            this.lblBin16 = new System.Windows.Forms.Label();
            this.lblBin128 = new System.Windows.Forms.Label();
            this.gbxCards = new System.Windows.Forms.GroupBox();
            this.pbxCard1 = new System.Windows.Forms.PictureBox();
            this.pbxCard2 = new System.Windows.Forms.PictureBox();
            this.pbxCard4 = new System.Windows.Forms.PictureBox();
            this.pbxCard8 = new System.Windows.Forms.PictureBox();
            this.pbxCard16 = new System.Windows.Forms.PictureBox();
            this.pbxCard32 = new System.Windows.Forms.PictureBox();
            this.pbxCard64 = new System.Windows.Forms.PictureBox();
            this.pbxCard128 = new System.Windows.Forms.PictureBox();
            this.gbxCalcul = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCalc2 = new System.Windows.Forms.Label();
            this.lblCalc4 = new System.Windows.Forms.Label();
            this.lblCalc1 = new System.Windows.Forms.Label();
            this.lblCalc8 = new System.Windows.Forms.Label();
            this.lblCalc16 = new System.Windows.Forms.Label();
            this.lblCalc32 = new System.Windows.Forms.Label();
            this.lblCalc64 = new System.Windows.Forms.Label();
            this.lblCalc128 = new System.Windows.Forms.Label();
            this.lblView128 = new System.Windows.Forms.Label();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.fichiersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outilsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmModes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManuel = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAuto = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCartes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRetourner = new System.Windows.Forms.ToolStripMenuItem();
            this.aideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblView64 = new System.Windows.Forms.Label();
            this.lblView32 = new System.Windows.Forms.Label();
            this.lblView2 = new System.Windows.Forms.Label();
            this.lblView16 = new System.Windows.Forms.Label();
            this.lblView8 = new System.Windows.Forms.Label();
            this.lblView4 = new System.Windows.Forms.Label();
            this.lblView1 = new System.Windows.Forms.Label();
            this.gbxResult = new System.Windows.Forms.GroupBox();
            this.lblCalcResult = new System.Windows.Forms.Label();
            this.lblMode = new System.Windows.Forms.Label();
            this.mainTimer = new System.Timers.Timer();
            this.gbxBin.SuspendLayout();
            this.gbxCards.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard128)).BeginInit();
            this.gbxCalcul.SuspendLayout();
            this.msMain.SuspendLayout();
            this.gbxResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.mainTimer)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxBin
            // 
            this.gbxBin.Controls.Add(this.lblBin1);
            this.gbxBin.Controls.Add(this.lblBin2);
            this.gbxBin.Controls.Add(this.lblBin4);
            this.gbxBin.Controls.Add(this.lblBin8);
            this.gbxBin.Controls.Add(this.lblBin32);
            this.gbxBin.Controls.Add(this.lblBin64);
            this.gbxBin.Controls.Add(this.lblBin16);
            this.gbxBin.Controls.Add(this.lblBin128);
            this.gbxBin.Location = new System.Drawing.Point(12, 55);
            this.gbxBin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxBin.Name = "gbxBin";
            this.gbxBin.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxBin.Size = new System.Drawing.Size(1232, 100);
            this.gbxBin.TabIndex = 0;
            this.gbxBin.TabStop = false;
            this.gbxBin.Text = "Nombre binaire correspondant";
            // 
            // lblBin1
            // 
            this.lblBin1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblBin1.Location = new System.Drawing.Point(1059, 30);
            this.lblBin1.Name = "lblBin1";
            this.lblBin1.Size = new System.Drawing.Size(143, 57);
            this.lblBin1.TabIndex = 8;
            this.lblBin1.Text = "1";
            this.lblBin1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBin2
            // 
            this.lblBin2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblBin2.Location = new System.Drawing.Point(911, 30);
            this.lblBin2.Name = "lblBin2";
            this.lblBin2.Size = new System.Drawing.Size(143, 57);
            this.lblBin2.TabIndex = 7;
            this.lblBin2.Text = "1";
            this.lblBin2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBin4
            // 
            this.lblBin4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblBin4.Location = new System.Drawing.Point(763, 30);
            this.lblBin4.Name = "lblBin4";
            this.lblBin4.Size = new System.Drawing.Size(143, 57);
            this.lblBin4.TabIndex = 6;
            this.lblBin4.Text = "1";
            this.lblBin4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBin8
            // 
            this.lblBin8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblBin8.Location = new System.Drawing.Point(615, 30);
            this.lblBin8.Name = "lblBin8";
            this.lblBin8.Size = new System.Drawing.Size(143, 57);
            this.lblBin8.TabIndex = 5;
            this.lblBin8.Text = "1";
            this.lblBin8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBin32
            // 
            this.lblBin32.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblBin32.Location = new System.Drawing.Point(319, 30);
            this.lblBin32.Name = "lblBin32";
            this.lblBin32.Size = new System.Drawing.Size(143, 57);
            this.lblBin32.TabIndex = 4;
            this.lblBin32.Text = "1";
            this.lblBin32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBin64
            // 
            this.lblBin64.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblBin64.Location = new System.Drawing.Point(171, 30);
            this.lblBin64.Name = "lblBin64";
            this.lblBin64.Size = new System.Drawing.Size(143, 57);
            this.lblBin64.TabIndex = 3;
            this.lblBin64.Text = "1";
            this.lblBin64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBin16
            // 
            this.lblBin16.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblBin16.Location = new System.Drawing.Point(467, 30);
            this.lblBin16.Name = "lblBin16";
            this.lblBin16.Size = new System.Drawing.Size(143, 57);
            this.lblBin16.TabIndex = 3;
            this.lblBin16.Text = "1";
            this.lblBin16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBin128
            // 
            this.lblBin128.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblBin128.Location = new System.Drawing.Point(23, 30);
            this.lblBin128.Name = "lblBin128";
            this.lblBin128.Size = new System.Drawing.Size(140, 57);
            this.lblBin128.TabIndex = 2;
            this.lblBin128.Text = "1";
            this.lblBin128.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbxCards
            // 
            this.gbxCards.Controls.Add(this.pbxCard1);
            this.gbxCards.Controls.Add(this.pbxCard2);
            this.gbxCards.Controls.Add(this.pbxCard4);
            this.gbxCards.Controls.Add(this.pbxCard8);
            this.gbxCards.Controls.Add(this.pbxCard16);
            this.gbxCards.Controls.Add(this.pbxCard32);
            this.gbxCards.Controls.Add(this.pbxCard64);
            this.gbxCards.Controls.Add(this.pbxCard128);
            this.gbxCards.Location = new System.Drawing.Point(12, 161);
            this.gbxCards.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxCards.Name = "gbxCards";
            this.gbxCards.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxCards.Size = new System.Drawing.Size(1232, 272);
            this.gbxCards.TabIndex = 1;
            this.gbxCards.TabStop = false;
            this.gbxCards.Text = "Cartes binaires";
            // 
            // pbxCard1
            // 
            this.pbxCard1.Image = global::CardBinaryWinForm.Resources._1;
            this.pbxCard1.Location = new System.Drawing.Point(1059, 32);
            this.pbxCard1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxCard1.Name = "pbxCard1";
            this.pbxCard1.Size = new System.Drawing.Size(107, 174);
            this.pbxCard1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxCard1.TabIndex = 10;
            this.pbxCard1.TabStop = false;
            this.pbxCard1.Click += new System.EventHandler(this.card_Click);
            // 
            // pbxCard2
            // 
            this.pbxCard2.Image = global::CardBinaryWinForm.Resources._2;
            this.pbxCard2.Location = new System.Drawing.Point(911, 32);
            this.pbxCard2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxCard2.Name = "pbxCard2";
            this.pbxCard2.Size = new System.Drawing.Size(107, 174);
            this.pbxCard2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxCard2.TabIndex = 9;
            this.pbxCard2.TabStop = false;
            this.pbxCard2.Click += new System.EventHandler(this.card_Click);
            // 
            // pbxCard4
            // 
            this.pbxCard4.Image = global::CardBinaryWinForm.Resources._4;
            this.pbxCard4.Location = new System.Drawing.Point(763, 32);
            this.pbxCard4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxCard4.Name = "pbxCard4";
            this.pbxCard4.Size = new System.Drawing.Size(107, 174);
            this.pbxCard4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxCard4.TabIndex = 8;
            this.pbxCard4.TabStop = false;
            this.pbxCard4.Click += new System.EventHandler(this.card_Click);
            // 
            // pbxCard8
            // 
            this.pbxCard8.Image = global::CardBinaryWinForm.Resources._8;
            this.pbxCard8.Location = new System.Drawing.Point(615, 32);
            this.pbxCard8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxCard8.Name = "pbxCard8";
            this.pbxCard8.Size = new System.Drawing.Size(107, 174);
            this.pbxCard8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxCard8.TabIndex = 7;
            this.pbxCard8.TabStop = false;
            this.pbxCard8.Click += new System.EventHandler(this.card_Click);
            // 
            // pbxCard16
            // 
            this.pbxCard16.Image = global::CardBinaryWinForm.Resources._16;
            this.pbxCard16.Location = new System.Drawing.Point(467, 32);
            this.pbxCard16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxCard16.Name = "pbxCard16";
            this.pbxCard16.Size = new System.Drawing.Size(107, 174);
            this.pbxCard16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxCard16.TabIndex = 6;
            this.pbxCard16.TabStop = false;
            this.pbxCard16.Click += new System.EventHandler(this.card_Click);
            // 
            // pbxCard32
            // 
            this.pbxCard32.Image = global::CardBinaryWinForm.Resources._32;
            this.pbxCard32.Location = new System.Drawing.Point(319, 32);
            this.pbxCard32.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxCard32.Name = "pbxCard32";
            this.pbxCard32.Size = new System.Drawing.Size(107, 174);
            this.pbxCard32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxCard32.TabIndex = 5;
            this.pbxCard32.TabStop = false;
            this.pbxCard32.Click += new System.EventHandler(this.card_Click);
            // 
            // pbxCard64
            // 
            this.pbxCard64.Image = global::CardBinaryWinForm.Resources._64;
            this.pbxCard64.Location = new System.Drawing.Point(171, 32);
            this.pbxCard64.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxCard64.Name = "pbxCard64";
            this.pbxCard64.Size = new System.Drawing.Size(107, 174);
            this.pbxCard64.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxCard64.TabIndex = 4;
            this.pbxCard64.TabStop = false;
            this.pbxCard64.Click += new System.EventHandler(this.card_Click);
            // 
            // pbxCard128
            // 
            this.pbxCard128.Image = global::CardBinaryWinForm.Resources._128;
            this.pbxCard128.Location = new System.Drawing.Point(23, 32);
            this.pbxCard128.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxCard128.Name = "pbxCard128";
            this.pbxCard128.Size = new System.Drawing.Size(107, 174);
            this.pbxCard128.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxCard128.TabIndex = 3;
            this.pbxCard128.TabStop = false;
            this.pbxCard128.Click += new System.EventHandler(this.card_Click);
            // 
            // gbxCalcul
            // 
            this.gbxCalcul.Controls.Add(this.label9);
            this.gbxCalcul.Controls.Add(this.label8);
            this.gbxCalcul.Controls.Add(this.label7);
            this.gbxCalcul.Controls.Add(this.label6);
            this.gbxCalcul.Controls.Add(this.label5);
            this.gbxCalcul.Controls.Add(this.label4);
            this.gbxCalcul.Controls.Add(this.label3);
            this.gbxCalcul.Controls.Add(this.label2);
            this.gbxCalcul.Controls.Add(this.label1);
            this.gbxCalcul.Controls.Add(this.lblCalc2);
            this.gbxCalcul.Controls.Add(this.lblCalc4);
            this.gbxCalcul.Controls.Add(this.lblCalc1);
            this.gbxCalcul.Controls.Add(this.lblCalc8);
            this.gbxCalcul.Controls.Add(this.lblCalc16);
            this.gbxCalcul.Controls.Add(this.lblCalc32);
            this.gbxCalcul.Controls.Add(this.lblCalc64);
            this.gbxCalcul.Controls.Add(this.lblCalc128);
            this.gbxCalcul.Location = new System.Drawing.Point(12, 500);
            this.gbxCalcul.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxCalcul.Name = "gbxCalcul";
            this.gbxCalcul.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxCalcul.Size = new System.Drawing.Size(1232, 100);
            this.gbxCalcul.TabIndex = 1;
            this.gbxCalcul.TabStop = false;
            this.gbxCalcul.Text = "Calcul de la valeur décimale équivalente";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label9.Location = new System.Drawing.Point(1144, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 57);
            this.label9.TabIndex = 17;
            this.label9.Text = "=";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label8.Location = new System.Drawing.Point(1015, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 57);
            this.label8.TabIndex = 16;
            this.label8.Text = "+";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label7.Location = new System.Drawing.Point(868, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 57);
            this.label7.TabIndex = 15;
            this.label7.Text = "+";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label6.Location = new System.Drawing.Point(715, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 57);
            this.label6.TabIndex = 14;
            this.label6.Text = "+";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label5.Location = new System.Drawing.Point(575, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 57);
            this.label5.TabIndex = 13;
            this.label5.Text = "+";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label4.Location = new System.Drawing.Point(425, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 57);
            this.label4.TabIndex = 12;
            this.label4.Text = "+";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label3.Location = new System.Drawing.Point(575, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 57);
            this.label3.TabIndex = 12;
            this.label3.Text = "+";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label2.Location = new System.Drawing.Point(275, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 57);
            this.label2.TabIndex = 11;
            this.label2.Text = "+";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label1.Location = new System.Drawing.Point(119, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 57);
            this.label1.TabIndex = 10;
            this.label1.Text = "+";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCalc2
            // 
            this.lblCalc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblCalc2.Location = new System.Drawing.Point(911, 31);
            this.lblCalc2.Name = "lblCalc2";
            this.lblCalc2.Size = new System.Drawing.Size(143, 57);
            this.lblCalc2.TabIndex = 9;
            this.lblCalc2.Text = "1";
            this.lblCalc2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCalc4
            // 
            this.lblCalc4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblCalc4.Location = new System.Drawing.Point(763, 31);
            this.lblCalc4.Name = "lblCalc4";
            this.lblCalc4.Size = new System.Drawing.Size(143, 57);
            this.lblCalc4.TabIndex = 8;
            this.lblCalc4.Text = "1";
            this.lblCalc4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCalc1
            // 
            this.lblCalc1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblCalc1.Location = new System.Drawing.Point(1059, 31);
            this.lblCalc1.Name = "lblCalc1";
            this.lblCalc1.Size = new System.Drawing.Size(143, 57);
            this.lblCalc1.TabIndex = 8;
            this.lblCalc1.Text = "1";
            this.lblCalc1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCalc8
            // 
            this.lblCalc8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblCalc8.Location = new System.Drawing.Point(615, 31);
            this.lblCalc8.Name = "lblCalc8";
            this.lblCalc8.Size = new System.Drawing.Size(143, 57);
            this.lblCalc8.TabIndex = 7;
            this.lblCalc8.Text = "1";
            this.lblCalc8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCalc16
            // 
            this.lblCalc16.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblCalc16.Location = new System.Drawing.Point(467, 31);
            this.lblCalc16.Name = "lblCalc16";
            this.lblCalc16.Size = new System.Drawing.Size(143, 57);
            this.lblCalc16.TabIndex = 6;
            this.lblCalc16.Text = "1";
            this.lblCalc16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCalc32
            // 
            this.lblCalc32.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblCalc32.Location = new System.Drawing.Point(319, 31);
            this.lblCalc32.Name = "lblCalc32";
            this.lblCalc32.Size = new System.Drawing.Size(143, 57);
            this.lblCalc32.TabIndex = 5;
            this.lblCalc32.Text = "1";
            this.lblCalc32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCalc64
            // 
            this.lblCalc64.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblCalc64.Location = new System.Drawing.Point(171, 31);
            this.lblCalc64.Name = "lblCalc64";
            this.lblCalc64.Size = new System.Drawing.Size(143, 57);
            this.lblCalc64.TabIndex = 4;
            this.lblCalc64.Text = "1";
            this.lblCalc64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCalc128
            // 
            this.lblCalc128.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblCalc128.Location = new System.Drawing.Point(23, 31);
            this.lblCalc128.Name = "lblCalc128";
            this.lblCalc128.Size = new System.Drawing.Size(140, 57);
            this.lblCalc128.TabIndex = 3;
            this.lblCalc128.Text = "1";
            this.lblCalc128.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblView128
            // 
            this.lblView128.Location = new System.Drawing.Point(35, 450);
            this.lblView128.Name = "lblView128";
            this.lblView128.Size = new System.Drawing.Size(143, 23);
            this.lblView128.TabIndex = 4;
            this.lblView128.Text = "128";
            this.lblView128.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.fichiersToolStripMenuItem, this.outilsToolStripMenuItem, this.aideToolStripMenuItem});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.msMain.Size = new System.Drawing.Size(1268, 28);
            this.msMain.TabIndex = 2;
            this.msMain.Text = "menuStrip1";
            // 
            // fichiersToolStripMenuItem
            // 
            this.fichiersToolStripMenuItem.Name = "fichiersToolStripMenuItem";
            this.fichiersToolStripMenuItem.Size = new System.Drawing.Size(64, 24);
            this.fichiersToolStripMenuItem.Text = "Fichier";
            // 
            // outilsToolStripMenuItem
            // 
            this.outilsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.tsmModes, this.tsmCartes});
            this.outilsToolStripMenuItem.Name = "outilsToolStripMenuItem";
            this.outilsToolStripMenuItem.Size = new System.Drawing.Size(59, 24);
            this.outilsToolStripMenuItem.Text = "Outils";
            // 
            // tsmModes
            // 
            this.tsmModes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.tsmManuel, this.tsmAuto});
            this.tsmModes.Name = "tsmModes";
            this.tsmModes.Size = new System.Drawing.Size(123, 24);
            this.tsmModes.Text = "Modes";
            // 
            // tsmManuel
            // 
            this.tsmManuel.Name = "tsmManuel";
            this.tsmManuel.Size = new System.Drawing.Size(165, 24);
            this.tsmManuel.Text = "Manuel";
            this.tsmManuel.Click += new System.EventHandler(this.tsms_Click);
            // 
            // tsmAuto
            // 
            this.tsmAuto.Name = "tsmAuto";
            this.tsmAuto.Size = new System.Drawing.Size(165, 24);
            this.tsmAuto.Text = "Automatique";
            this.tsmAuto.Click += new System.EventHandler(this.tsms_Click);
            // 
            // tsmCartes
            // 
            this.tsmCartes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.tsmRetourner});
            this.tsmCartes.Name = "tsmCartes";
            this.tsmCartes.Size = new System.Drawing.Size(123, 24);
            this.tsmCartes.Text = "Cartes";
            // 
            // tsmRetourner
            // 
            this.tsmRetourner.Name = "tsmRetourner";
            this.tsmRetourner.Size = new System.Drawing.Size(172, 24);
            this.tsmRetourner.Text = "Tout retourner";
            this.tsmRetourner.Click += new System.EventHandler(this.tsms_Click);
            // 
            // aideToolStripMenuItem
            // 
            this.aideToolStripMenuItem.Name = "aideToolStripMenuItem";
            this.aideToolStripMenuItem.Size = new System.Drawing.Size(52, 24);
            this.aideToolStripMenuItem.Text = "Aide";
            // 
            // lblView64
            // 
            this.lblView64.Location = new System.Drawing.Point(183, 450);
            this.lblView64.Name = "lblView64";
            this.lblView64.Size = new System.Drawing.Size(143, 23);
            this.lblView64.TabIndex = 5;
            this.lblView64.Text = "64";
            this.lblView64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblView32
            // 
            this.lblView32.Location = new System.Drawing.Point(331, 450);
            this.lblView32.Name = "lblView32";
            this.lblView32.Size = new System.Drawing.Size(143, 23);
            this.lblView32.TabIndex = 6;
            this.lblView32.Text = "32";
            this.lblView32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblView2
            // 
            this.lblView2.Location = new System.Drawing.Point(923, 450);
            this.lblView2.Name = "lblView2";
            this.lblView2.Size = new System.Drawing.Size(143, 23);
            this.lblView2.TabIndex = 7;
            this.lblView2.Text = "2";
            this.lblView2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblView16
            // 
            this.lblView16.Location = new System.Drawing.Point(479, 450);
            this.lblView16.Name = "lblView16";
            this.lblView16.Size = new System.Drawing.Size(143, 23);
            this.lblView16.TabIndex = 7;
            this.lblView16.Text = "16";
            this.lblView16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblView8
            // 
            this.lblView8.Location = new System.Drawing.Point(627, 450);
            this.lblView8.Name = "lblView8";
            this.lblView8.Size = new System.Drawing.Size(143, 23);
            this.lblView8.TabIndex = 8;
            this.lblView8.Text = "8";
            this.lblView8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblView4
            // 
            this.lblView4.Location = new System.Drawing.Point(775, 450);
            this.lblView4.Name = "lblView4";
            this.lblView4.Size = new System.Drawing.Size(143, 23);
            this.lblView4.TabIndex = 9;
            this.lblView4.Text = "4";
            this.lblView4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblView1
            // 
            this.lblView1.Location = new System.Drawing.Point(1071, 450);
            this.lblView1.Name = "lblView1";
            this.lblView1.Size = new System.Drawing.Size(143, 23);
            this.lblView1.TabIndex = 10;
            this.lblView1.Text = "1";
            this.lblView1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbxResult
            // 
            this.gbxResult.Controls.Add(this.lblCalcResult);
            this.gbxResult.Location = new System.Drawing.Point(12, 615);
            this.gbxResult.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxResult.Name = "gbxResult";
            this.gbxResult.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxResult.Size = new System.Drawing.Size(1232, 100);
            this.gbxResult.TabIndex = 2;
            this.gbxResult.TabStop = false;
            this.gbxResult.Text = "Résultat du calcul";
            // 
            // lblCalcResult
            // 
            this.lblCalcResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblCalcResult.Location = new System.Drawing.Point(5, 41);
            this.lblCalcResult.Name = "lblCalcResult";
            this.lblCalcResult.Size = new System.Drawing.Size(1221, 57);
            this.lblCalcResult.TabIndex = 9;
            this.lblCalcResult.Text = "1";
            this.lblCalcResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMode
            // 
            this.lblMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblMode.Location = new System.Drawing.Point(12, 734);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(1232, 33);
            this.lblMode.TabIndex = 10;
            this.lblMode.Text = "Mode Manuel";
            this.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mainTimer
            // 
            this.mainTimer.Interval = 1000D;
            this.mainTimer.SynchronizingObject = this;
            this.mainTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.mainTimer_Elapsed);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 776);
            this.Controls.Add(this.lblMode);
            this.Controls.Add(this.gbxResult);
            this.Controls.Add(this.lblView1);
            this.Controls.Add(this.lblView4);
            this.Controls.Add(this.lblView8);
            this.Controls.Add(this.lblView16);
            this.Controls.Add(this.lblView2);
            this.Controls.Add(this.lblView32);
            this.Controls.Add(this.lblView64);
            this.Controls.Add(this.lblView128);
            this.Controls.Add(this.gbxCards);
            this.Controls.Add(this.gbxCalcul);
            this.Controls.Add(this.gbxBin);
            this.Controls.Add(this.msMain);
            this.MainMenuStrip = this.msMain;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmMain";
            this.Text = "Cartes binaires 3.0 - Représentation des nombres binaires";
            this.gbxBin.ResumeLayout(false);
            this.gbxCards.ResumeLayout(false);
            this.gbxCards.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard4)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard8)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard16)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard32)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard64)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbxCard128)).EndInit();
            this.gbxCalcul.ResumeLayout(false);
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.gbxResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.mainTimer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Timers.Timer mainTimer;

        private System.Windows.Forms.ToolStripMenuItem tsmCartes;
        private System.Windows.Forms.ToolStripMenuItem tsmRetourner;

        private System.Windows.Forms.ToolStripMenuItem tsmAuto;
        private System.Windows.Forms.ToolStripMenuItem tsmManuel;
        private System.Windows.Forms.ToolStripMenuItem tsmModes;

        private System.Windows.Forms.Label lblMode;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;

        private System.Windows.Forms.Label lblCalc1;
        private System.Windows.Forms.Label lblCalc128;
        private System.Windows.Forms.Label lblCalc16;
        private System.Windows.Forms.Label lblCalc2;
        private System.Windows.Forms.Label lblCalc32;
        private System.Windows.Forms.Label lblCalc4;
        private System.Windows.Forms.Label lblCalc64;
        private System.Windows.Forms.Label lblCalc8;
        private System.Windows.Forms.Label lblCalcResult;

        private System.Windows.Forms.GroupBox gbxResult;
        private System.Windows.Forms.Label lblBin16;
        private System.Windows.Forms.Label lblBin64;
        private System.Windows.Forms.Label lblBin32;
        private System.Windows.Forms.Label lblBin8;
        private System.Windows.Forms.Label lblBin4;
        private System.Windows.Forms.Label lblBin2;
        private System.Windows.Forms.Label lblBin1;

        private System.Windows.Forms.Label lblView64;
        private System.Windows.Forms.Label lblView32;
        private System.Windows.Forms.Label lblView2;
        private System.Windows.Forms.Label lblView16;
        private System.Windows.Forms.Label lblView8;
        private System.Windows.Forms.Label lblView4;
        private System.Windows.Forms.Label lblView1;
        private System.Windows.Forms.PictureBox pbxCard64;
        private System.Windows.Forms.PictureBox pbxCard32;
        private System.Windows.Forms.PictureBox pbxCard16;
        private System.Windows.Forms.PictureBox pbxCard8;
        private System.Windows.Forms.PictureBox pbxCard4;
        private System.Windows.Forms.PictureBox pbxCard2;
        private System.Windows.Forms.PictureBox pbxCard1;

        private System.Windows.Forms.ToolStripMenuItem aideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outilsToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem fichiersToolStripMenuItem;
        private System.Windows.Forms.MenuStrip msMain;

        private System.Windows.Forms.Label lblBin128;
        private System.Windows.Forms.Label lblView128;
        private System.Windows.Forms.PictureBox pbxCard128;

        private System.Windows.Forms.GroupBox gbxBin;
        private System.Windows.Forms.GroupBox gbxCards;
        private System.Windows.Forms.GroupBox gbxCalcul;

        #endregion
    }
}