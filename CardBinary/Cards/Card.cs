﻿using System.Drawing;

namespace CardBinary.Cards
{
    public class Card
    {
        private readonly CardManager _manager;

        public Card(CardManager manager, int value, Image baseImage)
        {
            _manager = manager;
            Value = value;
            BaseImage = baseImage;
            Retourner = false;
        }

        public Card(CardManager manager, int value) : this(manager, value, null)
        {
        }

        public int Value { get; }

        public Image BaseImage { get; set; }

        public bool Retourner { get; set; }

        public Image CurrentImage => !Retourner ? BaseImage : (Image) _manager.ObjectRetourner;
    }
}