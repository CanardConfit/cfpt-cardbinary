﻿using System.Collections.Generic;

namespace CardBinary.Cards
{
    public abstract class CardManager
    {
        public abstract int ActualValue { get; }
        public abstract Mode ActualMode { get; set; }
        public abstract List<Card> Cards { get; }
        public abstract object ObjectRetourner { get; }
        public abstract object RetourneCard(int value);
        public abstract void RetourneAll();
    }

    public class Mode
    {
        public static Mode Automatique = new("Mode Automatique");
        public static Mode Manuel = new("Mode Manuel");

        private Mode(string mode)
        {
            ModeName = mode;
        }

        public string ModeName { get; }

        public override string ToString()
        {
            return ModeName;
        }
    }
}